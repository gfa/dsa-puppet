module Puppet::Parser::Functions
  newfunction(:gen_dkim_entry, :type => :rvalue) do |args|
    keyfile = args.shift()
    selector = args.shift()
    hostname = args.shift()
    domain = args.shift()

    res = []

    if keyfile.start_with?('---')
      # a string containing a key
      pubkey = keyfile.split("\n").select { |line| not line.start_with?('---') }
      keyfile = "<raw key>"
    else
      # an on-disk key
      pubkey = function_extract_public_key([keyfile]).split("\n")
    end

    res << "; cert #{keyfile}, selector #{selector} for #{hostname}."

    unless pubkey[0].start_with?(';')
      res << "#{selector}.#{hostname}._domainkey.#{domain}.\tIN\tTXT\t(\"v=DKIM1; k=rsa; s=email; h=sha256; p=\""
      pubkey.each do |line|
        res << "\"#{line}\"" unless line.start_with?("---")
      end
      res << ")\n"
      res << "#{selector}.#{hostname}._domainkey.#{hostname}.#{domain}.\tIN\tCNAME\t#{selector}.#{hostname}._domainkey.#{domain}.\n"
    else
      res += pubkey
    end

    return res.join("\n")
  end
end
