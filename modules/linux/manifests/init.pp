# common bits for all linux hosts
class linux {
  include ferm
  include ferm::per_host
  include entropykey
  include rng_tools
  if $::hostname in [fasolo, storace, sallinen, csail-node01, csail-node02, schumann, wieck] {
    $blacklist_acpi_power_meter = true
  } else {
    $blacklist_acpi_power_meter = false
  }
  if $blacklist_acpi_power_meter {
    file { '/etc/modprobe.d/hwmon.conf':
      content => 'blacklist acpi_power_meter'
    }
  }
}
