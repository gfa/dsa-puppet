# A debian.org buildd
class buildd {
  # Do nothing until we get the buildd user from ldap
  if $::buildd_user_exists {
    # home directory
    file { '/home/buildd':
      ensure  => directory,
      mode    => '2755',
      group   => buildd,
      owner   => buildd,
    }

    include buildd::schroot
    include buildd::dupload
    include buildd::aptitude
    include buildd::gnupg
    include buildd::ssh

    if $::hostname in [x86-grnet-01,x86-grnet-02,zani] {
      include buildd::pybuildd
    } else {
      include buildd::buildd
    }
  }
}
