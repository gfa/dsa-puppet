# create an ssl key pair
# @param directory where to create the key; defaults to "/etc/ssl/private"
# @param algorithm what sort of key to create; defaults to "rsa"
# @param keyfile   filename for the on-disk key
define ssl::keygen(
  String $directory = '/etc/ssl/private',
  String $algorithm = 'rsa',
  String $keyfile = $name,
) {
  $filename = "${directory}/${keyfile}.key"
  exec { "create-${algorithm}-ssl-key-${keyfile}":
    command => "openssl genpkey -algorithm ${algorithm} -out ${filename}",
    creates => [$filename],
    umask   => '0277',
  }
}
