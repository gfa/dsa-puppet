# Set up NFS mounts via autofs
class autofs {
  case $::hostname {
    picconi, sor, respighi: {
      include autofs::bytemark
    }
    lw07,lw08: {
      include autofs::leaseweb
    }
    tye,ullmann,piu-slave-ubc-01,hier,manziarly,lindsay,pinel,ticharich,donizetti,mekeel,pejacevic,delfin, coccia, quantz: {
      include autofs::ubc
    }
    default: {}
  }
}
