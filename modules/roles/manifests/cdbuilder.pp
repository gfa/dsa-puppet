class roles::cdbuilder {
  # debian-cd wants to make hardlinks to files it doesn't own; let it.
  file { '/etc/sysctl.d/protect-links.conf':
    ensure => absent,
  }
}
