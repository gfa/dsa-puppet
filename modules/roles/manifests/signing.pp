# code signing for EFI secure boot
class roles::signing {
  package { 'expect': ensure => installed, }
  package { 'pesign': ensure => installed, }
  package { 'linux-kbuild-4.19': ensure => installed, }
  package { 'libengine-pkcs11-openssl': ensure => installed, }
}
