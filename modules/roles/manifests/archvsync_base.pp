# this is pulled in by *-mirror or syncproxy roles
# in ensures the archvsync user has a home, and
# that mirrormaster can ssh to it
class roles::archvsync_base {
  file { '/srv/mirrors':
    ensure => directory,
    owner  => root,
    group  => 'archvsync',
    mode   => '0775',
  }

  file { '/srv/mirrors/.nobackup':
    ensure  => present,
    content => '',
  }

  file { '/etc/ssh/userkeys/archvsync':
    ensure => 'link',
    target => '/home/archvsync/.ssh/authorized_keys',
  }

  Ferm::Rule::Simple <<| tag == 'ssh::server::to::archvsync' |>>
}
