class roles::api_ftp_master {
  include apache2
  ssl::service { 'api.ftp-master.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  # is api_ftp_master the right role to put this in?
  include roles::udd::db_guest_access
}
