# the jenkins.debian.org CI system
class roles::jenkins {
  include roles::sso_rp

  include apache2
  include apache2::auth_digest
  include apache2::authn_file
  include apache2::proxy_http
  include apache2::rewrite
  include apache2::ssl

  apache2::site { '010-jenkins.debian.org':
    site   => 'jenkins.debian.org',
    source => 'puppet:///modules/roles/jenkins/jenkins.debian.org',
  }

  ssl::service { 'jenkins.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  dsa_systemd::linger { 'jenkins': }

  file { '/etc/sudoers.d/jenkins':
    mode   => '0440',
    source => 'puppet:///modules/roles/jenkins/sudoers',
  }

  ensure_packages([
    'debian.org-jenkins.debian.org'
  ], { ensure => 'installed' })


  # home directories
  file { [
      '/srv/jenkins.debian.org',
      '/srv/jenkins.debian.org/home-adm'
    ]:
      ensure => directory,
      mode   => '2755',
      owner  => 'jenkins-adm',
      group  => 'jenkins-adm',
  }
  file { '/home/jenkins-adm':
    ensure => link,
    target => '/srv/jenkins.debian.org/home-adm',
  }
  file { '/srv/jenkins.debian.org/home-unpriv':
      ensure => directory,
      mode   => '2755',
      owner  => 'jenkins',
      group  => 'jenkins',
  }
  file { '/home/jenkins':
    ensure => link,
    target => '/srv/jenkins.debian.org/home-unpriv',
  }
}
