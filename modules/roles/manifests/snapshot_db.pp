# db server providing snapshot databases
#
# @param db_port          port of the snapshot cluster
# @param guest_addresses  addresses to allow for the guest account
# @param upstream_db_server  if this node is a replica, name of the upstream db server
# @param upstream_db_port    if this node is a replica, port of the upstream db server
# @param upstream_db_role    if this node is a replica, replication role on the upstream db server
class roles::snapshot_db (
  Integer $db_port,
  Array[Stdlib::IP::Address] $guest_addresses = ['127.0.0.1', '::1'],
  Optional[String] $upstream_db_server = undef,
  Optional[Integer] $upstream_db_port = undef,
  String $upstream_db_role = "repuser-${::hostname}",
) {
  $now = Timestamp()
  $date = $now.strftime('%F')

  if versioncmp($::lsbmajdistrelease, '9') <= 0 {
    $ensure = 'absent'
  } elsif versioncmp($date, '2020-03-31') <= 0 {
    $ensure = 'present'
  } else {
    $ensure = 'absent'
    notify {'Temporary old pg ignore rule expired, clean up puppet':
      loglevel => warning,
    }
  }
  file { '/etc/nagios/obsolete-packages-ignore.d/puppet-postgres':
    ensure  => $ensure,
    content => @(EOF),
      libperl5.24:amd64
      postgresql-client-9.6
      postgresql-contrib-9.6
      perl-modules-5.24
      postgresql-plperl-9.6
      postgresql-9.6-debversion
      libgdbm3:amd64
      postgresql-9.6
      | EOF
  }


  postgres::cluster::hba_entry { 'snapshot-guest':
    pg_port  => $db_port,
    database => 'snapshot',
    user     => 'guest',
    address  => $guest_addresses,
    method   => 'trust',
    order    => '20',
  }

  if $upstream_db_server {
    if !$upstream_db_port {
      fail ('Also need a port if we have a upstream_db_server')
    }
    @@postgres::cluster::hba_entry { "snapshot-replica-to-${::fqdn}":
      tag      => "postgres::cluster::${upstream_db_port}::hba::${upstream_db_server}",
      pg_port  => $upstream_db_port,
      database => 'replication',
      user     => $upstream_db_role,
      address  => $base::public_addresses,
    }
  }
}
