class haveged {
	$ensure = ($::haveged) ? {
		true    => 'present',
		default => 'absent'
	}


	if ($haveged) {
		service { 'haveged':
			ensure => running,
		}
	}

	# work around #858134
	dsa_systemd::override { 'haveged':
		ensure => $ensure,
		content => @(EOT)
			[Unit]
			After=systemd-tmpfiles-setup.service
			| EOT
	}
}
